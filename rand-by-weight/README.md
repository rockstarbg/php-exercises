Here's some basic usage of the file you'll need to create:

> Retrieves random value off of an array based on given weight map

```php
$values = [50, 20, 10, 45, 18, 25];
$weights = [0, 10, 5, 30, 15, 12];

$results = array_fill_keys($values, 0);

for ($i = 0; $i < 10000; $i++) {
    $value = weightedRandom($values, $weights);

    $results[$value]++;
}

arsort($results);

var_dump($results === [45, 18, 25, 20, 10, 50]); // outputs: `bool(true)`
```

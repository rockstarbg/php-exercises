<?php

require('index.php');

class RandomByWeight extends \PHPUnit\Framework\TestCase
{
    public function testWeightRandom()
    {
        $values = [50, 20, 10, 45, 18, 25];
        $weights = [0, 10, 5, 30, 15, 12];

        $results = array_fill_keys($values, 0);

        for ($i = 0; $i < 10000; $i++) {
            $value = weightedRandom($values, $weights);

            $results[$value]++;
        }

        arsort($results); // Sort by value, maintaining key association

        // Should be 45 since it has the highest weight
        $this->assertEquals(key($results), 45);
    }

    public function testDynamicNumberWeight()
    {
        $values = $results = [];
        $weightMap = [10, 1, 30, 60, 40, 20];

        for($i = 0; $i < count($weightMap); $i++)
        {
            $values[] = mt_rand(100, 1000);
        }

        // Prefill the array with zeroes
        $results = array_fill_keys($values, 0);

        for($i = 0; $i < 10000; $i++)
        {
            $value = weightedRandom($values, $weightMap);

            $results[$value]++;
        }

        // Sort by value, maintaining key association
        arsort($results);

        $this->assertEquals($values[3], key($results));
    }
}

# PHP Exercises
> Rockstar.bg's interview questions

It's using the TDD approach so make sure to check the test.php file inside each exercise directory to find out what exactly needs to be done.
Write your solution as the index.php file, inside the exercise directory and run the tests using PHPUnit. Solve only what you can, these tests are representative of your knowledge. Thank you!

### Dependencies:
PHPUnit: https://phpunit.de/manual/current/en/installation.html

### Workflow:
```bash
[~]             $ git clone git@bitbucket.org:rockstarbg/php-exercises.git
[~]             $ cd php-exercises
[php-exercises] $ cd once
[once]          $ touch index.php
[once]          $ phpunit test.php
```

### PHPUnit Installation & Testing
```bash
[~]             $ composer install
[~]             $ ./vendor/bin/phpunit rand-by-weight/test.php
```

When you reach **OK** in your tests you can send them over to your recruiter or you can submit your changes as a pull-request to the repository. 
